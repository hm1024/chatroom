package org.example.util;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

//websocket的配置类：建立连接之前，定义一个配置
public class WebsocketConfigurator extends ServerEndpointConfig.Configurator {

    //进行我手阶段的一些配置：在客户端和服务端建立websocket连接时
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        //定义好配置，websocket建立连接时，先执行配置的代码
        HttpSession session = (HttpSession) request.getHttpSession();
        //保存HttpSession信息
        if(session != null){
            sec.getUserProperties().put("HttpSession",session);
        }
    }
}
