package org.example.util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {

    //定义一个单例的数据源/连接池对象
    //饿汉式
//    private static volatile MysqlDataSource DS = new MysqlDataSource();


    //双重校验锁的单例对象
    private static volatile MysqlDataSource DS = null;

    public static MysqlDataSource getDS() {
        if (DS==null){
            synchronized (MysqlDataSource.class){
                if(DS==null){
                    DS = new MysqlDataSource();
                    DS.setUrl("jdbc:mysql://127.0.0.1/chatroom");
                    DS.setUser("root");
                    DS.setPassword("root");
                    DS.setUseSSL(false);
                    DS.setUseUnicode(true);
                    DS.setCharacterEncoding("utf-8");
                }
            }
        }
        return DS;
    }

    static {
        getDS();
    }

//    static {
//        DS.setUrl("jdbc:mysql://127.0.0.1/chatroom");
//        DS.setUser("root");
//        DS.setPassword("root");
//        DS.setUseSSL(false);
//        DS.setUseUnicode(true);
//        DS.setCharacterEncoding("utf-8");
//    }

    public static Connection getConnection(){
        try {
            return DS.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("获取数据库连接失败",e);
        }
    }
    public static void close(Connection c,Statement s){
        close(c,s,null);
    }

    public static void close(Connection c, Statement s, ResultSet rs){
        try {
            if(rs != null) rs.close();
            if(s != null) s.close();
            if(c != null) c.close();
        } catch (SQLException e) {
            throw new RuntimeException("jdbc释放数据库资源出错");
        }
    }

    @Test
    public void getConnectionTest(){
        System.out.println(getConnection());
    }
}
