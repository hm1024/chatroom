package org.example.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 数据库实体类：
 * http请求可能把请求数据转换为一个实体类对象
 * http响应可能把实体类对象/List<实体类>返回客户端
 * 数据库操作使用实体类对象来操作
 * 查询返回一个或多个实体类
 */
@Setter
@Getter
@ToString
public class User {
    private Integer id;
    private String username;
    private String password;
    private String nickname;
    private String head;
    private java.util.Date logoutTime;
}
