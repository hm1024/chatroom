package org.example.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Message {
    private Integer id;
    private Integer userId;
    private String userNickname;
    private Integer groupId;
    private String content;
    private java.util.Date sendTime;
}
