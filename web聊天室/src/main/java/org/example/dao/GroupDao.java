package org.example.dao;

import org.example.model.Group;
import org.example.util.DBUtil;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GroupDao {
    public static List<Group> selectAll() {
        Connection c = null;
        Statement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select * from chat_group order by id";
            ps = c.createStatement();
            rs = ps.executeQuery(sql);
            List<Group> groups = new ArrayList<>();
            while(rs.next()){
                Group group = new Group();
                int id = rs.getInt("id");
                String name = rs.getString("name");
                group.setId(id);
                group.setName(name);
                groups.add(group);
            }
            return groups;
        } catch (SQLException e) {
            throw new RuntimeException("查询群组列表jdbc出错",e);
        } finally {
            DBUtil.close(c,ps,rs);
        }
    }

    @Test
    public void selectAllTest(){
        System.out.println(selectAll());
    }
}
