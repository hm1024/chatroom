package org.example.api;

import org.example.dao.GroupDao;
import org.example.model.Group;
import org.example.model.User;
import org.example.util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.MarshalledObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/groupList")
public class groupListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回群组列表及登录用户信息
        //校验是否登录
        User loginUser = WebUtil.getLoginUser(req.getSession(false));
        if(loginUser == null){
            resp.setStatus(403);
            return;
        }

        //登录成功，获取群组列表数据
        List<Group> groups = GroupDao.selectAll();
        Map<String,Object> map = new HashMap<>();
        map.put("user",loginUser);
        map.put("groups",groups);

        //返回相应数据
        String body = WebUtil.write(map);
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(body);
    }
}
