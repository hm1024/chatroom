package org.example.api;

import org.example.dao.UserDao;
import org.example.model.JsonResult;
import org.example.model.User;
import org.example.util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        User input = WebUtil.read(req.getInputStream(), User.class);
        User exist = UserDao.checkIfExist(input.getUsername(),null);
        JsonResult result = new JsonResult();
        if(exist == null){
            result.setReason("账号不存在");
        }else{
            if(!exist.getPassword().equals(input.getPassword())){
                result.setReason("密码错误");
            }else{
                //校验成功：创建session保存用户
                HttpSession session = req.getSession();
                session.setAttribute("user",exist);
                result.setOk(true);
            }

        }
        resp.setContentType("application/json; charset=utf-8");
        String body = WebUtil.write(result);
        resp.getWriter().write(body);
    }

}
