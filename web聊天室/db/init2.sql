drop database if exists chatroom;
create database chatroom character set utf8mb4;

use chatroom;

create table user(
    id int primary key auto_increment,
    username varchar(20) not null unique comment '账号',
    password varchar(50) not null comment '密码',
    nickname varchar(20) not null comment '昵称',
    head varchar(50) comment '头像路径，相对路径',
    logout_time datetime comment '用户上次注册时间'
) comment '用户名';


insert into user(username, password, nickname) values ('1','1','张三');
insert into user(username, password, nickname) values ('2','2','李四');
insert into user(username, password, nickname) values ('3','3','王五');

insert into user(username, password, nickname) values ('test','test','Admin');



create table chat_group(
    id int primary key auto_increment,
    name varchar(20) not null comment '群组名称'
)comment '群组';

insert into chat_group(name) values ('数学');
insert into chat_group(name) values ('语文');
insert into chat_group(name) values ('英语');
insert into chat_group(name) values ('政治');


create table message(
    id int primary key auto_increment,
    user_id int comment '用户id',
    user_nickname varchar(20) comment '消息发送人昵称',
    group_id int comment '群组名称',
    content varchar(255) not null comment '消息内容',
    send_time datetime comment '发送时间',
    foreign key (user_id) references user(id),
    foreign key (group_id) references chat_group(id)
)comment '群组消息记录';


UPDATE chatroom.user t SET t.logout_time = null WHERE t.id = 4;






